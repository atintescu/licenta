class AddPackageTypeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :package_type, :string
  end
end
