class Addserviceindetoappointment < ActiveRecord::Migration
  def change
        add_reference :appointments, :service, index: true
  end
end
