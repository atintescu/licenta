class Addfieldstousertable < ActiveRecord::Migration
  def change
    add_column :users, :title, :string
    add_column :users, :email, :string
    add_column :users, :password_salt, :string
    add_column :users, :password_hash, :string
    add_column :users, :user_type, :integer, defaul: 0
  end
end
