class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :cui
      t.string :phone
      t.string :address
      t.integer :tva
      t.integer :iduser

      t.timestamps
    end
  end
end
