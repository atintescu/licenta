class AddProfitabilityToServices < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :profitability, :string
  end
end
