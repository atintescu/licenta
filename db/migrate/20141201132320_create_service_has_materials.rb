class CreateServiceHasMaterials < ActiveRecord::Migration
  def change
    create_table :service_has_materials do |t|
      t.references :service, index: true
      t.references :material, index: true
      t.integer :quantity

      t.timestamps
    end
  end
end
