class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.references :user, index: true
      t.date :date
      t.time :time
      t.text :type
      t.integer :duration
      t.text :notes

      t.timestamps
    end
  end
end
