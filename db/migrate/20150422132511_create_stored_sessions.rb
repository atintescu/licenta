class CreateStoredSessions < ActiveRecord::Migration
  def change
    create_table :stored_sessions do |t|
      t.string :sid
      t.references :user, index: true

      t.timestamps
    end
  end
end
