class Inttofloat < ActiveRecord::Migration
  def up
    change_column :services, :duration, :float 
  end
  
  def down
    change_column :services, :duration, :integer
  end
end
