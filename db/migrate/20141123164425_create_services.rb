class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.references :user, index: true    
      t.text :service_name
      t.integer :duration
      t.integer :price    

      t.timestamps
    end
  end
end
