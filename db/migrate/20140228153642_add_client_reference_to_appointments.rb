class AddClientReferenceToAppointments < ActiveRecord::Migration
  def change
    add_reference :appointments, :client, index: true
  end
end
