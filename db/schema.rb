# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170616115812) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appointments", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "date"
    t.time     "time"
    t.text     "appointment_type"
    t.integer  "duration"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id"
    t.integer  "service_id"
    t.index ["client_id"], name: "index_appointments_on_client_id", using: :btree
    t.index ["service_id"], name: "index_appointments_on_service_id", using: :btree
    t.index ["user_id"], name: "index_appointments_on_user_id", using: :btree
  end

  create_table "clients", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "telephone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "emailC"
    t.index ["user_id"], name: "index_clients_on_user_id", using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "cui"
    t.string   "phone"
    t.string   "address"
    t.integer  "tva"
    t.integer  "iduser"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "materials", force: :cascade do |t|
    t.string   "name"
    t.string   "unit"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.float    "price"
    t.index ["user_id"], name: "index_materials_on_user_id", using: :btree
  end

  create_table "service_has_materials", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "material_id"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["material_id"], name: "index_service_has_materials_on_material_id", using: :btree
    t.index ["service_id"], name: "index_service_has_materials_on_service_id", using: :btree
  end

  create_table "services", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "service_name"
    t.float    "duration"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "profitability"
    t.index ["user_id"], name: "index_services_on_user_id", using: :btree
  end

  create_table "stored_sessions", force: :cascade do |t|
    t.string   "sid"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], name: "index_stored_sessions_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "telephone_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.string   "email"
    t.string   "password_salt"
    t.string   "password_hash"
    t.integer  "user_type"
    t.string   "package_type"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "auth_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
  end

end
