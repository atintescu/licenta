class ServiceHasMaterial < ActiveRecord::Base
  belongs_to :service
  belongs_to :material
end
