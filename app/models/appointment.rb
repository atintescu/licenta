class Appointment < ActiveRecord::Base
  belongs_to :user
  belongs_to :client
  has_one :service
  
  accepts_nested_attributes_for :client

  # after_create :reminder

  # @@REMINDER_TIME = 30.minutes # minutes before appointment

  # # Notify our appointment attendee X minutes before the appointment time
  # def reminder
  #   client = Twilio::REST::Client.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN'])

  #   #time_str = ((self.time).localtime).strftime("%I:%M%p on %b. %d, %Y")
  #   reminder = "Hi. Just a reminder that you have an appointment coming up at ."

  #   client.account.messages.create(
  #     from: ENV['TWILIO_NUMBER'],
  #     to: '+40741413061',
  #     body: '"Your appointment from"'
  #   )
  # end

  # def when_to_run
  #   self.time - @@REMINDER_TIME
  # end

  # handle_asynchronously :reminder, :run_at => Proc.new { |i| i.when_to_run }
  
end
