class Client < ActiveRecord::Base

    has_many :appointments

    def name
        name = first_name + ' ' + last_name
    end        
    
    def self.search_clients(search)
        search_item = search.downcase
        if search_item && search_item != ''
            Client.where(last_name: '#{search_item}')
        else
            []
        end
    end


end
