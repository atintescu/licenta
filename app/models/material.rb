require 'csv'

class Material < ActiveRecord::Base
	
	has_many :service_has_materials	

	def self.to_csv(options ={})

    attributes=%w{name unit price}
  	CSV.generate(options) do |csv|
  		csv << attributes
  		all.each do |material|
  			csv << material.attributes.values_at(*attributes)
  		end
  	end
  end

end
