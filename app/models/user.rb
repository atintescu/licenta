class User < ActiveRecord::Base

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, 
                          :url =>"/assets/users/:id/:style/:basename.:extension",
                          :path =>":rails_root/public/assets/users/:id/:style/:basename.:extension"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  require 'rubygems'
  
  include BCrypt

  has_many :appointments
  has_many :services	 

  
  validates :email, presence: true
  validates_uniqueness_of :email
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i


  # post '/receive_sms' do 
  #   content_type 'text/xml'

  #   response=Twilio::TwiML::Response.new do |r|
  #     r.Message 'Hey'
  #   end
  #    response.to_xml
  # end

  def send_message(message, app, delay)	

  	@day_and_time = app.date.to_s + " " + app.time.to_formatted_s(:time)	
  	@new_time = app.time + (delay.to_i).minutes		
  	
  	@pacient = Client.find_by_id(app.client_id)
          @phone = '+4' + @pacient.telephone
  	
  	@link = "http://assistant.herokuapp.com/appointment/" + app.id.to_s + "/" + @pacient.first_name + "/" + @pacient.last_name       
    if (delay.to_i).minutes !=0
      @body='"Your appointment from ' + @day_and_time + ' has been rescheduled at ' +  @new_time.to_formatted_s(:time) + '. Notice: ' + message 
                    #+'". You can find more details at this web link: ' + @link
    else
      @body='"Your appointment   has been set at ' +  @new_time.to_formatted_s(:time) + '. Notice: ' + message 
                     #+'". You can find more details at this web link: ' + @link
  	end 
    client = Twilio::REST::Client.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN'])
          client.account.messages.create(
          from: ENV['TWILIO_NUMBER'],
          to: @phone,
          body: @body
          )
  end
        
  def password
    if password_hash 
      @password ||= Password.new(password_hash)
    else
      @password = nil 
    end
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.password_hash = @password
  end

  def self.authenticate(email_param, password_param)
    if @user = find_by_email(email_param)
     if @user.password == password_param
     		@user
     	else
     	  nil
     end
    else
     nil
    end
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

end
