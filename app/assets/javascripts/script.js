function createAppointment(){
    console.log('ok');
}   

$(document).on('page:load', onPageLoad);
$(document).ready(onPageLoad);

function onPageLoad(){
    if($(".selectpicker").length != 0){
        $(".selectpicker").selectpicker();    
    }   
    
    $('.on-send-loading').click(function(){
        original_title = '<i class="fa fa-refresh fa-spin"></i> Sending...';
        $(this).html(original_title);
        $(this).addClass('disabled disable');
    });
}