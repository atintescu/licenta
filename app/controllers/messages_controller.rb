class MessagesController < ApplicationController

    def send_sms
		#the user will be chosen by an identifier
        user = User.find_or_create_by(first_name: 'Ana', last_name: 'Tintescu')
        message = params[:text]
        delay = params[:delay]

        #day = params[:appointment_day] 
		id_app = params[:appointment_id]        
		phone_number = '+4' + params[:pacient_telephone].to_s
		only1sms_value = params[:chkOnly1sms]        	

		@app = Appointment.find_by_id(id_app)
		@old_time  = @app.time
       
        if user.send_message(message[:content], @app, delay) 
            @ok = 1

	 	    #update pe programare doar daca a mers sms-ul
		    @app.time = @app.time + (delay.to_i).minutes
		    @app.save	  

		    if (only1sms_value != "1")
		    	#tb sa trimitem si la restul sms + update
			@apps = Appointment.where(date: @app.date, user_id: @app.user_id).where("time > ?", @old_time).where("id != ?", @app.id)
			
			if @apps.any?
				@apps.each do | ap |							
					if user.send_message(message[:content], ap, delay) 
						   ap.time = ap.time + (delay.to_i).minutes			
						   ap.save	
					end				
				end	
			end	
	    end		

        else
            @ok = 0
        end
		
		#raise @ok.to_s
        respond_to do |format|
            format.html{ redirect_to :back }
        end
    end
    
    
end
