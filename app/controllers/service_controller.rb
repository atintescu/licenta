class ServiceController < ApplicationController

  def index
    @services= Service.all
  end
  
  def create
    @service = Service.new(service_params)
    @service.user_id = current_user.id
    if @service.save
        @ok = 1
    else
        @ok = 0
    end
    respond_to do |format|
        format.js{}
    end
  end
  
  def edit
    @service = Service.find_by_id(params[:id])
    @servicehasmaterial = ServiceHasMaterial.new
    
    @materials = Material.where(user_id: current_user[:id])   
    @servicehasmaterials = ServiceHasMaterial.all
        
    respond_to do |format|
        format.html{
            render 'edit'
        }
    end
  end
  
  def update
    @service = Service.find_by_id(params[:id])
    @ok = @service.update_attributes(service_params)
    respond_to do |format|
        format.js{}
    end 
  end
    
  def service_params
    params.require(:service).permit(:user_id, :service_name, :duration, :price) 
  end

 def add_material_to_service
    @servicehasmaterial = ServiceHasMaterial.new(servicematerials_params)
    
    if @servicehasmaterial.save
        @ok2 = 1
    else
        @ok2 = 0
    end
    respond_to do |format|
        format.js{}
    end
  end

 def servicematerials_params
    params.require(:service_has_material).permit(:service_id, :material_id, :quantity) 
  end    

end 
