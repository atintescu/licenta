class CompanyController < ApplicationController
    def index
        @company= Company.new
    end

    def add
        @company = Company.new
        respond_to do |format|
            format.html{ render 'add' }
        end
    end
    
    def create
        @company = Company.new(company_params)
        @company[:iduser] = current_user.id
        respond_to do |format|
            format.html{
                if @company.save
                    redirect_to dashboard_path
                else
                    redirect_to :back
                end
            }
        end
    end
    
    def edit
        @company = Company.find_by_iduser(current_user[:id])
        respond_to do |format|
            format.html{
                render 'edit'
            }
        end
    end
  
    def update
        @company = Company.find_by_iduser(current_user[:id])
        @ok = @company.update_attributes(company_params)
        respond_to do |format|
            format.html{
                if @ok
                    redirect_to dashboard_path
                else
                    redirect_to :back
                end
            }
        end
    end
        
    def company_params
        params.require(:company).permit(:user_id, :name, :cui, :tva, :address, :phone)
    end
end


