class MaterialController < ApplicationController
 
 def create
    @material = Material.new(material_params)
    @material.user_id = current_user.id
    if @material.save
        @ok = 1
    else
        @ok = 0
    end
    respond_to do |format|
        format.js{}
    end
  end
  
  def edit
    @material = Material.find_by_id(params[:id])
    
    respond_to do |format|
        format.html{
            render 'edit'
        }
    end
  end
  
  def update
    @material = Material.find_by_id(params[:id])
    @ok = @material.update_attributes(material_params)
    respond_to do |format|
        if @ok
          redirect_to materials_path
        else
          redirect_to materials_path
        end
    end 
  end
    
    
  def material_params
    params.require(:material).permit(:name, :unit, :price) 
  end	


end
