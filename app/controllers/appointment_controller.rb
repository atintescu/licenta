class AppointmentController < ApplicationController

  skip_before_filter :require_login, :only =>  [:view]

  def create
    @appointment = Appointment.new(appointment_params)
    @appointment.notes= ''
    @appointment[:user_id] = current_user.id
    if @appointment.save
        @ok = 1
    else
        @ok = 0
    end
    respond_to do |format|
        format.js{}
    end
  end
  
  def edit
    @appointment = Appointment.find_by_id(params[:id])
    @serv = Service.where(id:  @appointment.service_id).first
    puts(@serv)
    @clients = Client.all
    respond_to do |format|
        format.html{
            render 'edit'
        }
    end
  end
  
  def update
    @appointment = Appointment.find_by_id(params[:id])
    @client=@appointment.client
    @ok = @appointment.update_attributes(appointment_params)

    EmailsMailer.send_appointments(@client, @appointment).deliver_now
    respond_to do |format|
        format.html{ 
            redirect_to :back
        }
    end 
  end
  
  def view
    @appointment = Appointment.find_by_id(params[:id])
    @client = @appointment.client
    @company = Company.find_by_iduser(@appointment.user.id)
    if @client.first_name != params[:first_name] || @client.last_name != params[:last_name]
        @appointment = nil
    end
    respond_to do |format|
        format.html{ 
          render 'view'
        }
    end
  end

  def destroy
    @appointment = Appointment.find_by_id(params[:id])
    @appointment.destroy
    redirect_to :back
  end
    
    
  def appointment_params
    params.require(:appointment).permit(:client_id, :service_id, :appointment_type, :date, :time, :duration, :notes)
  end
end 
