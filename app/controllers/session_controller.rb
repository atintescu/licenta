class SessionController < ApplicationController
  
  skip_before_filter :require_login, except: [:buy_plan, :dashboard_evercookie]
        
  def register
    @user = User.new
    @user.email = params[:email]
    @status = 1

    if params[:email].empty?
        @status = -1
    elsif params[:password] == params[:password_confirmation]
      	    @user.password = params[:password] 
      else
              @status = 0
    end

    respond_to do |format|
      format.html{
        if @status == 1
      	  if @user.save 
            session[:tmp_notice_type] = 'success'
            session[:tmp_notice_message] = 'Welcome!'
            @status = 1
            @messages = "Account created succesfully!"


            stored_session = StoredSession.create!(sid: SecureRandom.hex, user_id: @user[:id])
            cookies[:sid] = stored_session[:sid]

            redirect_to add_company_path
          else
            session[:tmp_notice_type] = 'error'
            session[:tmp_notice_message] = ''
            @status = 0
            @messages = @user.errors  
            redirect_to :back                
          end
        elsif @status == -1
          @password_message = "Please insert an email!"
          redirect_to :back
        else
          @password_message = "Passwords don't match."
          redirect_to :back
        end
      }
    end
  end
  
  def login
  	user = User.authenticate(params[:email], params[:password])
    @usr = user
  	respond_to do |format|
      format.html{
      	if user 
      	    if !cookies[:sid].empty?
      	        stored_session = StoredSession.where(sid: cookies[:sid]).first
      	        stored_session[:user_id] = user[:id]
      	        stored_session.save!
      	    else
      	       stored_session = StoredSession.create(sid: SecureRandom.hex, user_id: user[:id])
                 cookies[:sid] = stored_session[:sid] 
      	    end
      		    #session[:user_id] = user[:id]
              @ok = 1
              redirect_to dashboard_path

              
       	else
            @ok = 0
            redirect_to :back
           
       	end
       	}
   	end
  end	
 
  def new_user
    respond_to do |format|
      format.html{
        render 'new_user.html.erb'
      }
    end
  end
  
  def new_session
    respond_to do |format|
        format.html{
            render 'new.html.erb'
        }
    end
  end

# new actions that use evercookie

  def register_evercookie
  	@user = User.new
  	@user.email = params[:email]
  	@status = 1
  	if params[:email].empty?
  	    @status = -1
  	elsif params[:password] == params[:password_confirmation]
      	    @user.password = params[:password] 
      else
              @status = 0
      end
  	respond_to do |format|
  	   format.js{
      	   if @status == 1
          	   if @user.save 
              	   @message = "Account created succesfully!"
              	   stored_session = StoredSession.create!(sid: SecureRandom.hex, user_id: @user[:id])
              	   @sid = stored_session[:sid]
                   #cookies[:sid] = stored_session[:sid]
                 else
              	   @status = 0
              	   @message = @user.errors 
                 end
              else
                  @message = "Error, please try again"
              end
  	   }
  	end
  end
  
  def login_evercookie
  	user = User.authenticate(params[:email], params[:password])
  	respond_to do |format|
        format.html{
          	if user 
          	    if cookies[:sid] && !cookies[:sid].empty?
          	        stored_session = StoredSession.where(sid: cookies[:sid]).first
          	        stored_session[:user_id] = user[:id]
          	        stored_session.save!
          	        redirect_to dashboard_path
          	    else
          	       stored_session = StoredSession.create!(sid: SecureRandom.hex, user_id: user[:id])
          	       redirect_to set_evercookie_path(stored_session[:sid])

                   #cookies[:sid] = stored_session[:sid] 
          	    end
              #session[:user_id] = user[:id]
           	else
                  redirect_to :back
           	end
       	}
   	end
  end	
 
  def new_user_evercookie
    respond_to do |format|
        format.html{
            render 'new_user_evercookie'
        }
    end
  end
  
  def new_session_evercookie
    respond_to do |format|
        format.html{
            render 'new_evercookie'
        }
    end
  end

  def dashboard_evercookie
        
    @client = Client.new
    @clients = Client.all.where(user_id: current_user[:id])
    
    @appointment = Appointment.new
    @appointments = Appointment.where(user_id: current_user[:id])
    
  	@service = Service.new        
  	@services = Service.where(user_id: current_user[:id])

  	@material = Material.new
  	@materials = Material.where(user_id: current_user[:id])

    respond_to do |format|
        format.html{ render 'dashboard_evercookie'}
    end
  end
  
  def set_evercookie
    @sid = params[:sid]
    render 'set_evercookie'
  end

  # for payments
  def buy_plan   
  end
  
  def buy_plan_token
        token_id = params[:token_id]
         
        Stripe.api_key = "sk_test_wg3jxaKk5jUuB9dZkSTdUVye"
        
        # Get the credit card details submitted by the form
        token = params[:token_id]
        package = params[:package]
        
        # Create the charge on Stripe's servers - this will charge the user's card
        begin
          charge = Stripe::Charge.create(
            :amount => package.to_i * 100, # amount in cents, again
            :currency => "eur",
            :card => token,
            :description => "payinguser@example.com"
          )
        rescue Stripe::CardError => e
          # The card has been declined
        end
        
        if charge.paid == true
            user = User.find_by_id(current_user.id)
            user.package_type = package
            user.save
            
            render :json => 1
        else 
            render :json => 0
        end
  end
  
  def logout
    #session[:user_id] = nil
      if !cookies[:sid].empty?
        stored_session = StoredSession.where(sid: cookies[:sid]).first
        if stored_session
            stored_session.delete
        end
      end
      cookies[:sid] = nil
      respond_to do |format|
         format.html{ redirect_to root_path, notice: 'Logged out.'
         }
      end
  end
 private

  def user_params
    params.require(:user).permit(:avatar)
  end
  
end
