class PublicPageController < ApplicationController
    
    skip_before_filter :require_login

    def index
        render 'index'
    end
    
    def newsletter
#         raise 'here'
        if Gibbon::Request.new(:api_key =>ENV["MAIL_CHIMP_API_KEY"]).lists(ENV["MAIL_CHIMP_DEFAULT_LIST_ID"]).
            members.create(
                body:{
                    :email_address => params[:subscribe_email],
                    status: "subscribed",
                    merge_fields:{FNAME: "jon", LNAME:"doe"}
                    }) 
            @ok = 1
        else
            @ok = 0
        end
        respond_to do |format|
            format.html{
                if @ok
                    redirect_to :back, notice: 'You have been added to the newsletter.'
                else
                    redirect_to :back, alert: 'Something went wrong. Try again!'
                end
            }
        end
    end
    
end
