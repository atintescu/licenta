class UserController < ApplicationController
    require 'uri'
    require 'csv'
    require 'decisiontree'

    
    skip_before_filter :require_login, only: [:search]
    
    def dashboard
        @client = Client.new
        @clients = Client.all.where(user_id: current_user[:id])
        
        @appointment = Appointment.new
        @appointments = Appointment.where(user_id: current_user[:id])
        
    	@service = Service.new        
    	@services = Service.where(user_id: current_user[:id])

    	@material = Material.new
    	@materials = Material.where(user_id: current_user[:id])

    	@month = params[:month]
        @year = params[:year]
            
        if @month && @year
            @time = Time.new(@year,@month)
        else 
            @time = Time.new()
        end
                    
        @appointments_month = Appointment.where('extract(month from date) = ? and extract(year from date) = ? and user_id = ?', @time.month, @time.year, current_user.id).order('created_at asc')        
        @appointments_stats = @appointments_month.group_by{ |appointment| appointment.date.to_date}
    
        @total_appointments_from_month = @appointments_month.count
            
        respond_to do |format|
            format.html{ render 'dashboard'}
        end
    end
    
    def search
        @search = params[:search]
        @clients = Client.search_clients(@search)        
                
        respond_to do |format|
            format.html {render 'search'}
        end 
    end
      
    def clients
        @client = Client.new
        @clients = Client.all.where(user_id: current_user[:id])
        respond_to do |format|
            format.html {}
        end
    end
    
    def services
        @service = Service.new
        @services = Service.where(user_id: current_user[:id])
        @time = Time.new()
        ser= Service.all.where(user_id: current_user[:id])
        app= Appointment.where('extract(month from date) = ? and user_id = ?', @time.month, current_user.id) 
        app2= Appointment.where('extract(month from date) = ? and user_id = ?', @time.month - 1, current_user.id) 
        attributes = ["Duration", "Price"]
        array=[];

        ser.each do | serv |                            
            partArray=[]
            partArray.push(serv.duration.to_i)
            partArray.push(serv.price)
            nr= app.where(:service_id => serv.id).count 
            nr2= app2.where(:service_id => serv.id).count 
            if( ((serv.price/serv.duration.to_i) >=2 && (nr+nr2)>=2) ||  (nr+nr2)>=10)
                partArray.push("profitable".to_s)
            else
                partArray.push("unprofitable".to_s)
            end
            array.push(partArray)

        end 

        dec_tree = DecisionTree::ID3Tree.new(attributes, array, "profitable".to_s, :continuous)
        dec_tree.train
         @val1= params[:dur].to_i
         @val2= params[:pric].to_i
         @okish=0

         puts(@val1)
        
        if @val1!=0
            @okish=1
            test = [@val1, @val2]

            puts(array)
            @decision = dec_tree.predict(test)
        end

        respond_to do |format|
            format.html {}
        end
    end
    
    def materials
        @material = Material.new
        @materials = Material.where(user_id: current_user[:id])
        respond_to do |format|
            format.html {}
            format.xls {send_data @materials.to_csv(col_sep: "\t")}
            format.csv {send_data @materials.to_csv}
        end
    end
    
    def stats
        @apps = Appointment.all.where(user_id: current_user[:id])

        @services= Service.all.where(user_id: current_user[:id])

        @materials= Material.all.where(user_id: current_user[:id])

        @serv_mat= ServiceHasMaterial.all

        @month = params[:month]
        @year = params[:year]
        
        if @month && @year
            @time = Time.new(@year,@month)
        else 
            @time = Time.new()
        end
                
        @appointments_month = Appointment.where('extract(month from date) = ? and extract(year from date) = ? and user_id = ?', @time.month, @time.year, current_user.id).order('created_at asc')        
        @appointments = @appointments_month.group_by{ |appointment| appointment.date.to_date}
    
        @total_appointments_from_month = @appointments_month.count   
    end

    def appList
         @apps= Appointment.all.where(user_id: current_user[:id])
    end

    def check
       

        respond_to do |format|
            format.html{}
         
        end
    end

end
