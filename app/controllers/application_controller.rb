class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user
  before_filter :require_login
  require 'csv'

  def current_user
    if cookies[:sid]
#       if 1 == 1
        stored_session = StoredSession.where(sid: cookies[:sid]).first
        if stored_session
            @current_user = User.find_by_id(stored_session[:user_id])
        end
    else
        @current_user = nil
    end
    @current_user
#   	@current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
  end
  
  private 

  def require_login
    unless logged_in?
      flash[:error] = "You must be logged in to access this section"
      redirect_to root_path # halts request cycle
    end
  end

  def logged_in?
    !!current_user
  end
  
end
