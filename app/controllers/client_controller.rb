class ClientController < ApplicationController

  def create
    @client = Client.new(client_params)
    @client.user_id = current_user.id
    if @client.save
        @ok = 1
    else
        @ok = 0
    end
    respond_to do |format|
        format.js{}
    end
  end
  
  def show
    @client = Client.find_by_id(params[:id])
    respond_to do |format|
        format.html{
            render 'client'
        }
    end
  end    
  
  def edit
    @client = Client.find_by_id(params[:id])
    respond_to do |format|
        format.html{
            render 'edit'
        }
    end
  end
  
  def update
    @client = Client.find_by_id(params[:id])
    @ok = @client.update_attributes(client_params)
    respond_to do |format|
        if @ok
          redirect_to clients_path
        else
          redirect_to :back
        end
    end 
  end

   def destroy
    @client = Client.find_by_id(params[:id])
    @client.destroy
    
    redirect_to :back
  end
      
  private

  def client_params
    params.require(:client).permit(:id, :first_name, :last_name, :telephone, :emailC)
  end
  
end
