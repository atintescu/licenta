class ApplicationMailer < ActionMailer::Base
  default from: 'assistant.mail.reminder@gmail.com'
  layout 'mailer'
end

