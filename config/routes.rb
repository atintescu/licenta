Assistant::Application.routes.draw do

  root 'public_page#index'

                #controller, metoda, path


# for subscription
  post '/newsletter' => 'public_page#newsletter', as: :newsletter
  
#search
  get '/search' => 'user#search', as: :search

#session pages 
  post '/register' => 'session#register', as: :register
  post '/login' => 'session#login', as: :login
  get '/logout' => 'session#logout', as: :logout 
  get '/new-user' => 'session#new_user', as: :new_user
  get '/new' => 'session#new', as: :new  
  get '/buy-plan' => 'session#buy_plan', as: :buy_plan
  post '/buy-plan-token' => 'session#buy_plan_token', as: :buy_plan_token
    
#session pages for evercookie
  post '/register-evercookie' => 'session#register_evercookie', as: :register_evercookie
  post '/login-evercookie' => 'session#login_evercookie', as: :login_evercookie
  get '/new-user-evercookie' => 'session#new_user_evercookie', as: :new_user_evercookie
  get '/new-evercookie' => 'session#new_evercookie', as: :new_evercookie
  get '/dashboard-evercookie' => 'session#dashboard_evercookie', as: :dashboard_evercookie
  get '/set-evercookie/:sid' => 'session#set_evercookie', as: :set_evercookie
    
# create companies    
  get '/company/add' => 'company#add', as: :add_company
  post '/company/create' => 'company#create', as: :create_company
  get '/company/edit/:id' => 'company#edit', as: :edit_company
  post '/company/update' => 'company#update', as: :update_company
      
  get '/dashboard' => 'user#dashboard', as: :dashboard
  get '/clients' => 'user#clients', as: :clients
  get '/services' => 'user#services', as: :services
  post '/import/services' => 'services#import', as: :services_import
  get '/materials' => 'user#materials', as: :materials
  get '/stats' => 'user#stats', as: :stats
  get '/appList' => 'user#appList', as: :appList
  get '/checkService' => 'user#services', as: :checkService

  
  post '/clients/create' => 'client#create', as: :create_client
  post '/appointments/create' => 'appointment#create', as: :create_appointment
  post '/send_sms' => 'messages#send_sms', as: :send_sms
  post '/send_reminder' => 'messages#reminder_sms', as: :reminder_sms
  
  get '/client/:id' => 'client#show', as: :client
  get '/edit-client/:id' => 'client#edit', as: :edit_client
  post '/update-client/:id' => 'client#update', as: :update_client
  
  get '/appointment/:id/:first_name/:last_name' => 'appointment#view', as: :view_appointment
  get '/edit-appointment/:id' => 'appointment#edit', as: :edit_appointment
  post '/update-appointment/:id' => 'appointment#update', as: :update_appointment
  delete '/delete-appointment/:id' => 'appointment#destroy', as: :delete_appointment

  post '/services/create' => 'service#create', as: :create_service
  get '/edit-service/:id' => 'service#edit', as: :edit_service
  post '/update-service/:id' => 'service#update', as: :update_service 

  post '/materials/create' => 'material#create', as: :create_material
  get '/edit-material/:id' => 'material#edit', as: :edit_material
  post '/update-material/:id' => 'material#update', as: :update_material

  post '/services/add_material_to_service' => 'service#add_material_to_service', as: :create_servicehasmaterials	

end
